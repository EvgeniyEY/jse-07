package ru.ermolaev.tm;

import ru.ermolaev.tm.constant.ITerminalConst;

import java.util.Scanner;

public class Application implements ITerminalConst {

    public static void main(final String[] args) {
        System.out.println("Welcome to task manager");
        if(parseArgs(args)) System.exit(0);
        while (true) {
            final Scanner scanner = new Scanner(System.in);
            final String command = scanner.nextLine().toUpperCase();
            parseArg(command);
        }
    }

    private static void parseArg(final String arg) {
        if (arg.isEmpty()) return;
        switch (arg) {
            case ITerminalConst.HELP:
                showHelp();
                break;
            case ITerminalConst.ABOUT:
                showAbout();
                break;
            case ITerminalConst.VERSION:
                showVersion();
                break;
            case ITerminalConst.EXIT:
                exit();
                break;
            default:
                System.out.println("Invalid parameter");
        }
    }

    private static boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0].toUpperCase();
        parseArg(arg);
        return true;
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(ITerminalConst.ABOUT + " - Show developer info.");
        System.out.println(ITerminalConst.VERSION + " - Show version info.");
        System.out.println(ITerminalConst.HELP + " - Display terminal commands.");
        System.out.println(ITerminalConst.EXIT + " - Exit from application.");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Evgeniy Ermolaev");
        System.out.println("E-MAIL: ermolaev.evgeniy.96@yandex.ru");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.1");
    }

    private static void exit() {
        System.exit(0);
    }

}
