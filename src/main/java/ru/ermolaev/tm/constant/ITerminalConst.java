package ru.ermolaev.tm.constant;

public interface ITerminalConst {

    String HELP = "HELP";

    String ABOUT = "ABOUT";

    String VERSION = "VERSION";

    String EXIT = "EXIT";

}
